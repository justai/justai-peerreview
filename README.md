This GitLab instance is for mutually anonymous peer review.
- **warrant-data.csv** Human coded data used for training and testing. 
- **tf-idf-var-code.R** TF-IDF feature engineering + SVM training, testing and inter-rater reliablity
- **spacy-aveloc.R** POS average location feature engineering  + SVM training, testing and inter-rater reliablity
- **fasttext.R** FastTest single-layer neural network training, testing, and inter-rater reliablity
- **non-binary-fasttext.R** Non-binary classificaiton version of fasttext.R
- **bert-svm-classifier.R** SVM training, testing, and inter-rater reliablity using pre-processed data with BERT feature engineering
- **bert-feature-engineering.R** MPI cluster-ready BERT feature engineering 